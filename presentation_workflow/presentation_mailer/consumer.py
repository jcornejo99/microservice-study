import json
import pika
import django
import os
import sys
import time
from pika.exceptions import AMQPConnectionError
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    name = message['presenter_name']
    title = message['title']
    send_mail(
        'Your presentation has been accepted',
        f'{name}, we\'re happy to tell you that your presentation {title} has been aceepted',
        'admin@conference.go',
        ['presenter_email'],
        fail_silently=False,

    )


# parameters = pika.ConnectionParameters(host='rabbitmq')
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue='presentation_approvals')
# channel.basic_consume(
#     queue='presentation_approvals',
#     on_message_callback=process_approval,
#     auto_ack=True,
# )
# channel.start_consuming()


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    name = message['name']
    title = message['title']
    send_mail(
        'Your presentation has been denied',
        f'{name}, unfortunately we are here to tell you that your presentation {title} has been denied',
        'admin@conference.go',
        ['presenter_email'],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
